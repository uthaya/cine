require 'test_helper'

class CinemasControllerTest < ActionController::TestCase
  setup do
    @cinema = cinemas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cinemas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cinema" do
    assert_difference('Cinema.count') do
      post :create, cinema: { Actor: @cinema.Actor, Actress: @cinema.Actress, Average_Vote: @cinema.Average_Vote, Director: @cinema.Director, Language: @cinema.Language, Movie_Image: @cinema.Movie_Image, Movie_Name: @cinema.Movie_Name, Picture: @cinema.Picture, Reaction_Gif: @cinema.Reaction_Gif, Release_Month: @cinema.Release_Month, Release_Year: @cinema.Release_Year, Vote: @cinema.Vote, cc1: @cinema.cc1, cc2: @cinema.cc2, cc3: @cinema.cc3, cc4: @cinema.cc4, comedian: @cinema.comedian }
    end

    assert_redirected_to cinema_path(assigns(:cinema))
  end

  test "should show cinema" do
    get :show, id: @cinema
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cinema
    assert_response :success
  end

  test "should update cinema" do
    put :update, id: @cinema, cinema: { Actor: @cinema.Actor, Actress: @cinema.Actress, Average_Vote: @cinema.Average_Vote, Director: @cinema.Director, Language: @cinema.Language, Movie_Image: @cinema.Movie_Image, Movie_Name: @cinema.Movie_Name, Picture: @cinema.Picture, Reaction_Gif: @cinema.Reaction_Gif, Release_Month: @cinema.Release_Month, Release_Year: @cinema.Release_Year, Vote: @cinema.Vote, cc1: @cinema.cc1, cc2: @cinema.cc2, cc3: @cinema.cc3, cc4: @cinema.cc4, comedian: @cinema.comedian }
    assert_redirected_to cinema_path(assigns(:cinema))
  end

  test "should destroy cinema" do
    assert_difference('Cinema.count', -1) do
      delete :destroy, id: @cinema
    end

    assert_redirected_to cinemas_path
  end
end
