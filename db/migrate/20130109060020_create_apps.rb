class CreateApps < ActiveRecord::Migration
  def change
    create_table :apps do |t|
      t.string :movie_name
      t.string :language
      t.binary :picture
      t.string :actor
      t.string :actress
      t.string :comedian
      t.string :director
      t.string :music
      t.string :director
      t.integer :release_year
      t.integer :release_month
      t.date :release_date
      t.integer :vote
      t.float :average_vote
      t.binary :movie_image
      t.binary :reaction_gif
      t.string :cc1
      t.string :cc2
      t.string :cc3
      t.binary :cc4

      t.timestamps
    end
  end
end
