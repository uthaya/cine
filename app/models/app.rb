class App < ActiveRecord::Base
  attr_accessible :actor, :actress, :average_vote, :cc1, :cc2, :cc3, :cc4, :comedian, :director, :director, :language, :movie_image, :movie_name, :music, :picture, :reaction_gif, :release_date, :release_month, :release_year, :vote
end
